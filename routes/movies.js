const KoaRouter = require('Koa-router');
const router = new KoaRouter();

const moviesController = require('../controller/moviesController');


router.get('/favicon.ico', () => {
  return 'your faveicon'
})
router.all('/', moviesController.firstPage);
router.get('/replaceall', moviesController.replaceAllPlotsView);
//2. Obtener todas las películas:
router.get('/:page', moviesController.renderAllMovies);
//1. Buscador de películas: API
router.post('/add', moviesController.addMovie);
router.get('/search/:title', moviesController.searchMovieByTitle);

//3. Buscar y reemplazar:
router.post('/replace', moviesController.replaceMovie);
router.post('/replaceallplots', moviesController.replaceAllPlots);



module.exports = router;

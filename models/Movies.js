const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2')

const ratingSchema = mongoose.Schema({
  Source: {
    type: String,
    required: true,
    trim: true,
  },
  Value: {
    type: String,
    required: true,
    trim: true,
  },
});
const movieSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true,
  },
  year: {
    type: Number,
    required: true,
    trim: true,
  },
  released: {
    type: String,
    required: true,
    trim: true,
  },
  genre: {
    type: String,
    required: true,
    trim: true,
  },
  director: {
    type: String,
    required: true,
    trim: true,
  },
  actors: {
    type: String,
    required: true,
    trim: true,
  },
  plot: {
    type: String,
    required: true,
    trim: true,
  },
  ratings: [ratingSchema],
  poster: {
    type: String,
    required: true,
    trim: true,
  },
  imdbid: {
    type: String,
    required: true,
    trim: true,
    unique: true
  }
});

movieSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('movies', movieSchema)
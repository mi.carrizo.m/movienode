// var express = require('express');
const Koa = require('Koa');
const KoaRouter = require('Koa-router');
const json = require('Koa-json');
const path = require('path');
const render = require('koa-ejs');
const conectarDB = require('./config/db');
const bodyParser = require('koa-bodyparser');
const app = new Koa();
// const router = new KoaRouter();

var router = require('./routes/movies');

conectarDB();

app.use(json());
app.use(bodyParser());

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'layout',
  viewExt: 'html',
  cache: false,
  debug: false,
});

app.use(router.routes()).use(router.allowedMethods());

// app.use(async (ctx) => {
//   ctx.body = { msg: 'Hello World' };
// });

app.listen(3000, () => console.log('Server started...'));

module.exports = app;

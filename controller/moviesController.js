var config = require('../config/db');
const Movie = require('../models/Movies');
const axios = require('axios');
const { movieURL, apiKey } = require('../config/api');



//2. Obtener todas las películas:
//Método GET
//Se deben devolver todas las películas que se han guardado en la BD.
//Si hay más de 5 películas guardadas en BD, se deben paginar los resultados de 5 en 5
//El número de página debe ir por header
const firstPage = async (ctx) => {
  ctx.redirect('/1');
}

const renderAllMovies = async (ctx) => {
  var page = 1
  if(typeof ctx.params.page !== 'undefined'){
    var page = parseInt(ctx.params.page)
  }
  const mongoMovies = await Movie.paginate({},{limit: 5, page: page});
  await ctx.render('index', {
    title: 'MovieNode:',
    mongoMovies: mongoMovies.docs,
    actualPage: mongoMovies.page,
    nextPage: mongoMovies.nextPage,
    totalPages: mongoMovies.totalPages,
    prevPage: mongoMovies.prevPage
  });
};

//1. Buscador de películas: API
// Método GET
// El valor a buscar debe ir en la URL de la API
// Adicionalmente puede ir un header opcional que contenga el año de la película.
// Almacenar en una BD Mongo, la siguiente info:
// Title
// Year
// Released
// Genre
// Director
// Actors
// Plot
// Ratings
// El registro de la película solo debe estar una vez en la BD.
// Devolver la información almacenada en la BD.

const addMovie = async (ctx) => {
  try {
    const movieExist = await Movie.find({imdbid: ctx.request.body.imdbID})
    if(movieExist.length > 0){
      console.log("movie exist")
      const mongoMovies = await Movie.find();
      await ctx.render('index', {
        title: 'MovieNode:',
        mongoMovies: mongoMovies,
      });
    }
    if(movieExist.length === 0){
      console.log("movie does not exist")
          const searchMovie = await axios.get(
            `${movieURL}?i=${ctx.request.body.imdbID}${apiKey}`
          );
          const newMovie = {
            title: searchMovie.data.Title,
            year: searchMovie.data.Year,
            released: searchMovie.data.Released,
            genre: searchMovie.data.Genre,
            director: searchMovie.data.Director,
            actors: searchMovie.data.Actors,
            plot: searchMovie.data.Plot,
            ratings: searchMovie.data.Ratings,
            poster: searchMovie.data.Poster,
            imdbid: searchMovie.data.imdbID
          }
          const movie = new Movie(newMovie);
          await movie.save();
          ctx.redirect('/');
    }
  } catch (error) {
    console.log(error)
  }
};

const searchMovieByTitle = async (ctx) => {
  try {
    const searchMovie = await axios.get(
      `${movieURL}?t=${ctx.params.title}${apiKey}`
    );
    return ctx.render('search', {
      title: 'Search omdbapi:',
      searchmovie: searchMovie.data,
    });
  } catch (error) {
    console.log(error);
  }
};

//3. Buscar y reemplazar:
// Método POST que reciba en el BODY un object como: P.E: {movie: star wars, find: jedi, replace: CLM Dev }
// Buscar dentro de la BD y obtener el campo PLOT del registro
// Al string del plot obtenido buscar la palabra enviada en el Body (find) y reemplazar todas sus ocurrencias por el campo enviado en el body (replace)
// Devolver el string con las modificaciones del punto anterior

const replaceMovie = async (ctx) => {
  const { find, replace, imdbID } = ctx.request.body;
  const movieToUpdate = await Movie.find({imdbid: imdbID})
  newPlot = movieToUpdate[0].plot.replace(find, replace)
  await Movie.updateOne(
    {imdbid: imdbID},
    { $set: {plot: newPlot }}
  )
  ctx.redirect('/');
};

const replaceAllPlotsView = async (ctx) => {
  await ctx.render('replaceall', {
    title: 'Replace Plot',
  });
}
const replaceAllPlots = async (ctx) => {
  const { find, replace } = ctx.request.body;
  await Movie.find().then(movies => {
    movies.forEach(async function(movie){
      newPlot = movie.plot.replace(find, replace)
      await Movie.updateOne(
        {imdbid: movie.imdbid},
        { $set: {plot: newPlot }}
      )
    })
  })
  ctx.redirect('/');
}

module.exports = {
  firstPage,
  renderAllMovies,
  addMovie,
  searchMovieByTitle,
  replaceMovie,
  replaceAllPlotsView,
  replaceAllPlots,
};
